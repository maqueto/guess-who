package main.java.com.manumaqueda.alexa.whoiswho.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;

import java.util.Optional;

import static com.amazon.ask.request.Predicates.requestType;

public class LaunchIntentHandler implements RequestHandler {
    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(requestType(LaunchRequest.class));
    }
    @Override
    public Optional<Response> handle(HandlerInput input) {
        String repromptText = "For instructions on how you can play, please say help me.";
        return input.getResponseBuilder().withSpeech("Welcome to the guess who game ..." +
                "Now just tell me when you are ready and we start a new game!")
                .withReprompt(repromptText).build();
    }
}
