package main.java.com.manumaqueda.alexa.whoiswho.handlers.attributes;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import main.java.com.manumaqueda.alexa.whoiswho.engine.GuessWho;

import java.util.Optional;

public class AttributeHandler implements RequestHandler {
    GuessWho engine;
    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return false;
        // here to put the right intents
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        engine = new GuessWho(GuessWho.retreiveGameData(
                (String) input.getAttributesManager().getPersistentAttributes().get("sessionId")));

        engine.performEndTryActions();
        return Optional.empty();
    }


}
