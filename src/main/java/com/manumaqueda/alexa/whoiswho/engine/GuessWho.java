package main.java.com.manumaqueda.alexa.whoiswho.engine;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import main.java.com.manumaqueda.alexa.whoiswho.model.GameData;
import main.java.com.manumaqueda.alexa.whoiswho.model.GameStatus;


public class GuessWho {
    private GameData gameData;

    public GuessWho(GameData gd){
        this.gameData = gd;
    }
    public void start(int numPersons){
        // grab random list from s3
        AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();
        // select the person to guess
        this.gameData = new GameData(null, null);
        //persist data in dynamodb
    }
    public static GameData retreiveGameData(String id){
        //get data from dynamo
        return null;
    }
    public boolean checkAttribute(String attribute){
        switch (attribute) {
            case "eyes-open":
                return this.gameData.getPerson2Guess().getEyes().isOpen();
            case "eyes-closed":
                return !this.gameData.getPerson2Guess().getEyes().isOpen();
        }
        return false;
    }
    public void performEndTryAction(){
        if(this.gameData.checkReachedMaxNameChecks() || this.gameData.checkReachedMaxTries()){
            this.gameData.setStatus(GameStatus.LOST);
        }
    }

}
