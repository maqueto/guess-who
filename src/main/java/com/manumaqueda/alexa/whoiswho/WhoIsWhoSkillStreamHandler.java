package main.java.com.manumaqueda.alexa.whoiswho;

import com.amazon.ask.Skill;
import com.amazon.ask.SkillStreamHandler;
import com.amazon.ask.Skills;
import main.java.com.manumaqueda.alexa.whoiswho.handlers.*;


public class WhoIsWhoSkillStreamHandler extends SkillStreamHandler {
   private static Skill getSkill() {
       return Skills.standard()
               .addRequestHandlers(
                       new NewGameIntentHandler(),
                       new CancelandStopIntentHandler(),
                       new SessionEndedRequestHandler(),
                        new HelpIntentHandler(),
                       new LaunchIntentHandler())
               .withSkillId("amzn1.ask.skill.9473019a-dc38-49bb-b5b5-e7dc88a765a8")
               .build();
   }

    public WhoIsWhoSkillStreamHandler() {
        super(getSkill());
    }
}
