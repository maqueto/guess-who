package main.java.com.manumaqueda.alexa.whoiswho.model;

public class Open extends BooleanAttribute{
    public Open(boolean op){
        this.setAttribute(op);
    }
    public boolean isOpen(){
        return check();
    }
}
