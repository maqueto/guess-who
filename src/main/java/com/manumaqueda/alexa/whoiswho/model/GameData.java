package main.java.com.manumaqueda.alexa.whoiswho.model;

import java.util.List;

public class GameData {
    private String id;
    private List<Person> persons;
    private Person person2Guess;
    private int numTries;
    private static final Integer MAX_TRIES = 20;
    private GameStatus status;
    private static final Integer MAX_NAME_CHECKS = 3;
    private int numNameChecks;

    public GameData(List<Person> persons, Person person2Guess) {
        this.persons = persons;
        this.person2Guess = person2Guess;
        this.numTries = 0;
        this.status = GameStatus.ONGOING;
        this.numNameChecks = 0;
    }
    public void increasenumTries(){
        this.numTries ++;
    }
    public boolean checkReachedMaxTries(){
        if(this.numTries > MAX_TRIES)
            return true;
        return false;
    }
    public boolean checkReachedMaxNameChecks(){
        if(this.numNameChecks > MAX_NAME_CHECKS)
            return true;
        else
            return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public Person getPerson2Guess() {
        return person2Guess;
    }

    public void setPerson2Guess(Person person2Guess) {
        this.person2Guess = person2Guess;
    }

    public Integer getNumTries() {
        return numTries;
    }

    public void setNumTries(Integer numTries) {
        this.numTries = numTries;
    }

    public static Integer getMaxTries() {
        return MAX_TRIES;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }
}
