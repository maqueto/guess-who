package main.java.com.manumaqueda.alexa.whoiswho.model;

public enum GameStatus {
    NOT_STARTED,ONGOING,WIN,LOST
}
