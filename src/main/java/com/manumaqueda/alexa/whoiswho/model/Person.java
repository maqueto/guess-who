package main.java.com.manumaqueda.alexa.whoiswho.model;

import com.amazonaws.services.dynamodbv2.xspec.B;

public class Person implements CheckAttribute{
    private String name;
    private Integer id;
    private Beard moustache;
    private Boolean beard;
    private Smile smile;
    private Eyes eyes;
    private Glasses glasses;
    private Hair hair;
    private Mouth mouth;
    private Boolean isHidden = true;

    @Override
    public boolean check(Object name) {
        if(this.name.equalsIgnoreCase((String)name))
            return true;
        else
            return false;
    }
    public Boolean checkById(Integer id){
        if(this.id.compareTo(id) == 0)
            return true;
        else
            return false;
    }
    static class Smile extends BooleanAttribute{
        public boolean smile(){
            return this.check();
        }
    }
    static class Moustache extends BooleanAttribute{
        public boolean moustache(){
            return this.check();
        }

    }
    static class Beard extends BooleanAttribute{
        public boolean beard(){
            return this.check();
        }

    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public Beard getMoustache() {
        return moustache;
    }

    public Boolean getBeard() {
        return beard;
    }

    public Smile getSmile() {
        return smile;
    }

    public Eyes getEyes() {
        return eyes;
    }

    public Glasses getGlasses() {
        return glasses;
    }

    public Hair getHair() {
        return hair;
    }

    public Mouth getMouth() {
        return mouth;
    }

    public Boolean getHidden() {
        return isHidden;
    }
}
