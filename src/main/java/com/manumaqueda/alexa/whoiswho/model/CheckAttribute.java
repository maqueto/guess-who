package main.java.com.manumaqueda.alexa.whoiswho.model;

public interface CheckAttribute {
    boolean check(Object attribute);
}
