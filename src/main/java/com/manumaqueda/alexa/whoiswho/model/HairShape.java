package main.java.com.manumaqueda.alexa.whoiswho.model;

public enum HairShape {
    STRAIGHT("straight"), CURLY("curly"),BOLD("bold");
    private String value;
    private HairShape(String value){
        this.value = value;
    }
    public String getValue(){
        return this.value;
    }
}
