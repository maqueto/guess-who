package main.java.com.manumaqueda.alexa.whoiswho.model;

public class AttributeColorC implements CheckAttribute {
    AttributeColor color;

    public AttributeColorC(AttributeColor color) {
        this.color = color;
    }

    @Override
    public boolean check(Object attribute) {
        AttributeColor color2Check = (AttributeColor)attribute;
        if(color2Check.equals(this.color))
            return true;
        else
            return false;
    }
}
