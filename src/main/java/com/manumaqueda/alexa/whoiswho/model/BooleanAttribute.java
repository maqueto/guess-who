package main.java.com.manumaqueda.alexa.whoiswho.model;

public class  BooleanAttribute <T extends BooleanAttribute> implements CheckAttribute{
    private boolean attribute;

    @Override
    public boolean check(Object attribute) {
        return this.attribute;
    }

    public boolean check(){
        return check(null);
    }
    public void setAttribute(boolean attr){
        this.attribute = attr;
    }
}
