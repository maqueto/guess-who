package main.java.com.manumaqueda.alexa.whoiswho.model;

public class Hair {
    private AttributeColorC color;
    private Length isLong;
    private HairShapeC hairShape;

    public Hair(AttributeColor color, Length isLong, HairShape shape) {
        this.color = new AttributeColorC(color);
        this.isLong = isLong;
        this.hairShape = new HairShapeC(shape);
    }

    static class Length extends BooleanAttribute{
        public boolean isLong(){
            return this.check();
        }
    }
    static class HairShapeC implements CheckAttribute{
        private HairShape shape;

        public HairShapeC(HairShape shape) {
            this.shape = shape;
        }
        @Override
        public boolean check(Object attribute) {
            if(this.shape.equals((HairShape)attribute))
                return true;
            else
                return false;
        }
    }

}
