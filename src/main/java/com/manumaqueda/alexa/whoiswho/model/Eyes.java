package main.java.com.manumaqueda.alexa.whoiswho.model;

public class Eyes extends Open{
    private Open open;
    private AttributeColorC color;
    public Eyes(boolean open, AttributeColor color){
        super(open);
        this.color = new AttributeColorC(color);
    }
}
